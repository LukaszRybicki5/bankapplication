package com.pay.model;
/*
Polaz danymi dotyczącymi adresu klienta
 */
public class Address {

    private String city;
    private String street;
    private int numberOfHouse;
    private String postalCode;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumberOfHouse() {
        return numberOfHouse;
    }

    public void setNumberOfHouse(int numberOfHouse) {
        this.numberOfHouse = numberOfHouse;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Address(String city, String street, int numberOfHouse, String postalCode) {

        this.city = city;
        this.street = street;
        this.numberOfHouse = numberOfHouse;
        this.postalCode = postalCode;
    }

    public Address() {
    }
}
