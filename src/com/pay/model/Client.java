package com.pay.model;

public class Client {

    private String firstName;
    private String lastName;
    private String accountNumber;
    private Address address;
    private Long accountBalance;
    private String accountLogin;
    private String accountPassword;

    public Client(String firstName, String lastName,
                  String accountNumber, Address address,
                  Long accountBalance, String accountLogin,
                  String accountPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNumber = accountNumber;
        this.address = address;
        this.accountBalance = accountBalance;
        this.accountLogin = accountLogin;
        this.accountPassword = accountPassword;
    }

    public Client() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Long getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Long accountBalance) {
        this.accountBalance = accountBalance;
    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", address=" + address +
                ", accountBalance=" + accountBalance +
                ", accountLogin='" + accountLogin + '\'' +
                ", accountPassword='" + accountPassword + '\'' +
                '}';
    }
}
