package com.pay;

import com.pay.controller.CreatingAccount;

import java.io.IOException;
import java.util.Scanner;

/*
Konsolowa aplikacja bankowa BankApp;
Użyteczności:
-zakładanie konta;
-usuwanie konta;
-przelewy między kontami;
-lokaty;
-kredyty;
-tworzenie historii przelewów;
-prosta baza danych oparta na plikach tekstowych;

 */
public class BankApp {
    public static void main(String[] args) throws IOException {

        Scanner input = new Scanner(System.in);
        CreatingAccount creatingAccount = new CreatingAccount();

        boolean optionFlag = true;

        while (optionFlag) {
            System.out.println("<--------------------------------->" +
                    "\n\tWitaj w systemie bankowym!!!" +
                    "\n<---------------------------------> " +
                    "\nWybierz co chcesz zrobić:" +
                    "\na) Założenie konta" +
                    "\nb) Logowanie do systemu");

            String optionChoice = input.nextLine();

            switch (optionChoice) {
                case "a":  //zakładamy nowe konto, nadajemy wartości polom z package model
                    creatingAccount.addingData();
                    System.out.println(creatingAccount.getListOfClients());
                    break;

                case "b":

                    break;
                default:
                    System.out.println("Błąd!!! \nWybierz opcje " +
                            "(wpisz małą literę a lub b w konsolę.");
                    break;
            }

        }

    }
}
