package com.pay.controller;

import com.pay.model.Address;
import com.pay.model.Client;

import java.util.ArrayList;
import java.util.Scanner;

/*
Nadajemy wartości polom z danymi klienta
 */
public class CreatingAccount {

    ArrayList<Client> listOfClients = new ArrayList<>();

    public ArrayList<Client> getListOfClients() {
        return listOfClients;
    }

    public void setListOfClients(ArrayList<Client> listOfClients) {
        this.listOfClients = listOfClients;
    }

    Scanner input = new Scanner(System.in);
    boolean optionFlag2 = true;

    public void addingData() {

        Client client = new Client();
        Address address = new Address();
        System.out.println(
                "<--------------------------------->" +
                        "\n\tWitaj w systemie tworzenia konta!!!" +
                        "\n\tPodaj swoje dane według poleceń:\n" +
                        "<--------------------------------->\n");

        while (optionFlag2) {
            System.out.println("Podaj swoje imie (Jeśli imie jest dwuczłonowe użyj myślnika):");
            String firstName = input.nextLine();
            client.setFirstName(firstName);

            System.out.println("Podaj swoje nazwisko (Jeśli imie jest dwuczłonowe użyj myślnika):");
            String lastName = input.nextLine();
            client.setLastName(lastName);


            System.out.println("Podaj swój adres\nPodaj miasto:");
            String city = input.nextLine();
            address.setCity(city);

            System.out.println("Podaj ulicę:");
            String street = input.nextLine();
            address.setStreet(street);

            System.out.println("Podaj kod pocztowy:");
            String postalCode = input.nextLine();
            address.setPostalCode(postalCode);

            System.out.println("Podaj numer domu:");
            int numberOfHouse = input.nextInt();
            address.setNumberOfHouse(numberOfHouse);


            Long accountBalance = 0L;
            client.setAccountBalance(accountBalance);

            System.out.println(
                    "<--------------------------------->" +
                            "\n\tTwój obecny stan konta równa się 0.00 zł.\n" +
                            "\n\tProponujemy dokonac wpłaty na konto.\n" +
                            "<--------------------------------->\n");

            listOfClients.add(client);
            optionFlag2 = false;
        }

    }


}
