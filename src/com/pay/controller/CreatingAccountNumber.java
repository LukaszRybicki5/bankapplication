package com.pay.controller;

import com.pay.model.Client;

/*
Tworzymy prosty generator numeru konta
opieramy się na liczbach pseudolosowych
 */
public class CreatingAccountNumber {

    private Client client;
//    private String accountNumber;

    /*
    Metoda zwraca 16 cyfr
     */
    public String getAccountNumber() {

        int wrap, wrap2;

        //pierwsze 8 cyfr numeru konta
        do {
            wrap = (int) (Math.random() * 10000000);
        } while ((wrap >= 10000000) && (wrap <= 99999999));

        //cyfry numeru konta 9 - 16
        do {
            wrap2 = (int) (Math.random() * 10000000);
        } while ((wrap2 >= 10000000) && (wrap2 <= 99999999 ));

        String wrapString = Integer.toString(wrap);
        String wrapString2 = Integer.toString(wrap2);

        StringBuilder stringBuilder = new StringBuilder(wrapString).append(wrap2);

        String accountNumber = stringBuilder.toString();

        return accountNumber;
    }

}



